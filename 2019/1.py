import math

with open("input-1", "r") as f:
    lines = [int(line.strip()) for line in f.readlines()]

def compute_fuel(mass):
    return (math.floor(mass / 3) - 2)

def compute_total_fuel(mass):
    fuel = compute_fuel(mass)
    if fuel > 0:
        return fuel + compute_total_fuel(fuel)
    else:
        return 0

print("Total: {}".format(
    (sum([compute_fuel(mass) for mass in lines]))))
 
print("Total 2: {}".format(
    (sum([compute_total_fuel(mass) for mass in lines]))))



