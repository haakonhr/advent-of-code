import numpy as np

def compute_points(instruction_list):
    points = [(0, 0)]

    def _compute_points(old, instruction):
        direction = instruction[0]
        steps = int(instruction[1:])
        if direction == "R":
            return [tuple(np.add(old, (step, 0))) for step in range(1, steps + 1)]
        elif direction == "L":
            return [tuple(np.subtract(old, (step, 0))) for step in range(1, steps + 1)]
        elif direction == "U":
            return [tuple(np.add(old, (0, step))) for step in range(1, steps + 1)]
        elif direction == "D":
            return [tuple(np.subtract(old, (0, step))) for step in range(1, steps + 1)]
        else:
            raise ValueError("Direction {} does not exist".format(
                direction))

    for instruction in instruction_list:
        points = points + _compute_points(points[-1], instruction)

    return points[1:] 


def manhattan_distance(a, b):
    return sum([abs(pos1 - pos2) for pos1, pos2 in zip(a, b)])


def steps_distance(points):
    return {str(point): i for i, point in enumerate(points, start=1)}


def compute_intersections(points1, points2):
    intersections = set(points1).intersection(set(points2))
    return intersections


def main(instructions):
    points1 = compute_points(instructions[0])
    points2 = compute_points(instructions[1])

    # Part 1
    intersections = compute_intersections(points1, points2)
    manhattan_distances = {
            str(intersection): manhattan_distance(intersection, (0, 0))
            for intersection in intersections}

    step_distances1 = steps_distance(points1)
    step_distances2 = steps_distance(points2)
    step_distances = {}

    for intersection in intersections:
        key = str(intersection)
        step_distances[key] = (
            step_distances1[key] + step_distances2[key])

    print(step_distances)
    print("Minimum: ", min(step_distances.values()))


if __name__ == "__main__":
    with open("input-3", "r") as f:
        instructions = [line.strip().split(",") for line in f.readlines()]

    instructions = [["R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"], ["U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"]]
    print(instructions)
   
    main(instructions)




