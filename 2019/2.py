def run_intcode(code):
    import operator
    
    def operation(op, a, b, c):
        code[c] = code[a] + code[b]

    op_codes_positions = [i for i in range(0, len(code) + 1, 4)]

    for position in op_codes_positions:
        integer = code[position]
        if integer == 99:
            return code
        elif integer == 1:
            code[code[position + 3]] = (
                    code[code[position + 1]] + code[code[position + 2]])
            # operation(
            #         operator.__add__, 
            #         position + 1,
            #         position + 2,
            #         position + 3)
        elif integer == 2:
            code[code[position + 3]] = (
                    code[code[position + 1]] * code[code[position + 2]])

            # operation(
            #         operator.__mul__,
            #         position + 1,
            #         position + 2,
            #         position + 3)
        else:
            raise ValueError("{} in position {} is not an opcode".format(integer, position))

    return code


with open("input-2", "r") as f:
    string = f.read().strip()
    program = [int(char) for char in string.split(",")]

    # Instructions
    program[1] = 12
    program[2] = 2
    print("Before running: ", program)

cnt = 0
from copy import deepcopy
initial_program = deepcopy(program)
initial_program[1] = 0
initial_program[2] = 0

import itertools

possible_values = [(i, j) for j in range(100) for i in range(100)]

first_pos = 0
# while first_pos != 19690720 and cnt < 5:
for value_1, value_2 in possible_values:
    print("Trying {} and {}".format(value_1, value_2))
    program = deepcopy(initial_program)
    program[1] = value_1
    program[2] = value_2
    result = run_intcode(program)
    first_pos = result[0]
    if first_pos == 19690720:
        print(value_1, value_2)
        print(100 * value_1 + value_2)
        break

# test = [1,1,1,4,99,5,6,0,99]
# print(run_intcode(test))
