import networkx as nx
import matplotlib.pyplot as plt

"""It would also be an option to use topological sorting and then
to use a shortest path algorithm on that for better runtimes"""

test_input = """COM)B
                B)C
                C)D
                D)E
                E)F
                B)G
                G)H
                D)I
                E)J
                J)K
                K)L
                K)YOU
                I)SAN"""

if __name__=="__main__":
    # Parse into graph
    with open("input-6", "r") as f:
        lines = [line.strip() for line in f.readlines()]
        # lines = [line.strip() for line in test_input.split("\n")]
        E = [tuple(line.split(")")) for line in lines]
        V = list(set([v for edge in E for v in edge]))
        adjacency_list = {node: [v for u,v in E if u == node] for node in V}

    G = nx.Graph()
    G.add_nodes_from(V)
    G.add_edges_from(E)
    # plt.subplot(121)
    # nx.draw(g)
    # plt.show()
    shortest_paths = nx.single_source_shortest_path(G, "COM")
    print("Shortest paths:\n", shortest_paths)

    # Part 1
    orbit_count = sum(
            [len(value) - 1 for key, value in
             shortest_paths.items() if key != "COM"])
    print("Orbit count: {}".format(orbit_count))

    # Part 2
    shortest_paths_from_us = nx.single_source_shortest_path_length(G, "YOU")
    print(shortest_paths_from_us)
    orbit_transfers = shortest_paths_from_us["SAN"] - 2
    # -2 because we transfer orbits from the object we start orbiting
    # around to the object SAN is orbiting around.
    print("Orbit transfers: {}".format(orbit_transfers))

    

