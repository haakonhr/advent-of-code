#! /usr/bin/awk -f

function compute_fuel_v1(mass)
{
    remainder = mass % 3
    fuel = ((mass - remainder) / 3) - 2
    return fuel
}

function compute_fuel_v2(mass)
{
    fuel = compute_fuel_v1(mass)
    if (fuel > 0)
    {
        return fuel + compute_fuel_v2(fuel)
    }
    else
    {
        return 0
    }
}

# How does the context work in awk?
# I called the returned result from compute_fuel_v1 for fuel,
# but this was clearly overwritten by the fuel computed in
# compute_fuel_v2! 
# It just shows the need to understand scope rules.

BEGIN {print "part 1"}{
    input = $1
    fuel_v1 = compute_fuel_v1(input)
    fuel_v2 = compute_fuel_v2(input)
    total_v1 += fuel_v1
    total_v2 += fuel_v2}
END{print "Version 1: ", total_v1, "\nVersion 2: ", total_v2}


