(load "utils.lisp")
(ql:quickload 'uiop)

(defparameter *test* '(199 200 208 210 200 207 240 269 260 263))

;; Part 1
(defun find-increases (lst)
  (let ((len (length lst)))
    (mapcar (lambda (x y) (if (> y x)
			      1
			      0))
	    (slice lst 0 (1- len))
	    (slice lst 1 len))))

(defun day1-part1 (depths)
  (sum (find-increases depths)))

;; Part 2

;; Let's create the averages and make lists
(defun sliding-average (lst window-size)
  (loop for i
	from 0
	  to (- (length lst) window-size)
	collect (sum (slice lst i (+ i window-size)))
	))

(defun day1-part2 (depths)
  (let ((avg-depths (sliding-average depths 3)))
    (sum (find-increases avg-depths))))

;; Script
(defparameter *input* (mapcar #'parse-integer (uiop:read-file-lines "day1")))
