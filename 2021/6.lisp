(ql:quickload 'uiop)
(ql:quickload 'cl-ppcre)
(load "utils.lisp")

(defparameter *test* '(3 4 3 1 2))

(defun parse-input (file)
  (mapcar #'parse-integer (ppcre:all-matches-as-strings "(\\d+)"
							(uiop:read-file-line file))))

(defparameter *input* (parse-input "6-input"))

(defun process-timer (timer)
  (if (= timer 0)
      6
      (1- timer)))

(defun process-timers (timers)
  (let ((new-timers (mapcar #'process-timer timers))
	(num-of-spawns (count 0 timers)))
    (append new-timers (loop for i from 0 below num-of-spawns
			     collect 8))))

(defun simulate (initial-state days)
  (if (= days 0)
      initial-state
      (simulate (process-timers initial-state) (1- days))))

;; Dette blir kaotisk da vi ikke prosesserer dag for dag!

(defun process-day-ht (table)
  (let ((new-fish (make-hash-table)))
    (loop for timer being each hash-key of table
	    using (hash-value num-fish)
	  do (if (= 0 timer)
		 (progn
		   (incf (gethash 6 new-fish 0) num-fish)
		   (incf (gethash 8 new-fish 0) num-fish))
		 (progn
		   (incf (gethash (1- timer) new-fish 0) num-fish)
		   )))
    new-fish))

(defun create-initial-state (timers)
  (let* ((table (make-hash-table :test #'equal)))
    (loop for timer in timers
	  do (incf (gethash timer table 0) 1))
    table))

(defun simulate (timer-table days)
  (if (> days 0)
      (simulate (process-day-ht timer-table) (1- days))
      timer-table))

;; (defun simulate (initial-state days)
;;   (let* ((table (make-hash-table :test #'equal)))
;;     (loop for timer in initial-state
;; 	  do (incf (gethash timer table 0) 1))
;;     (loop for i from 0 below days
;; 	  do (let ((new-fish process-day-ht) table))
;; 	  finally )
;;     table))

    ;; (loop for timer being each hash-key of initial-state
    ;; 	    using (hash-value num-fish)
;; 	  do (incf (gethash timer table 0) 1))

(defun part2 (input)
  (let ((initial-state (create-initial-state input)))
    (sum (alexandria:hash-table-values (simulate initial-state 256)))))
