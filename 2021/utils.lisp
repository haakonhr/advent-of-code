(ql:quickload 'alexandria)

(defun combinations (&rest lists)
  (if (endp lists)
      (list nil)
      (mapcan (lambda (inner-val)
		(mapcar (lambda (outer-val)
			  (cons outer-val
				inner-val))
			(car lists)))
	      (apply #'combinations (cdr lists)))))

(defparameter *combos* (list))

(defun combinations (list)
  (alexandria:map-combinations (lambda (lst) (push lst *combos*)) list :length 2))

(defun get-n-items (lst num)
  (when (> num 0)
    (cons (car lst) (get-n-items (cdr lst) (1- num)))))

(defun slice (lst a b)
  (let ((count (- b a)))
    (if (> a 0)
	(slice (cdr lst) (1- a) (1- b))
	(get-n-items lst count))))

(defun sum (lst)
  (if (cdr lst)
      (+ (car lst) (sum (cdr lst)))
      (car lst)))

(defun char->binary (char) (- (char-int char) 48))
