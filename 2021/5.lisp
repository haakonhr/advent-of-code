(ql:quickload 'uiop)
(ql:quickload 'cl-ppcre)
(load "utils.lisp")

;; Parsing
;; We need to parse the first line as a list that we will run through. This is the bingo draws. We also need to parse the boards and store them in some manner.

(defun parse-lines (file)
  (mapcar (lambda (line)
            (mapcar #'parse-integer
                    (ppcre:all-matches-as-strings "(\\d+)" line)))
	  (uiop:read-file-lines file)))

(defparameter *test* (parse-lines "5-test"))
(defparameter *input* (parse-lines "5-input"))

;; Data structures
(defstruct point
  (x)
  (y))

(defstruct line-segment
  (start)
  (end))

(defun create-line-segments (pairs-of-points)
  (mapcar #'(lambda (points)
	      (let ((start-point (make-point :x (car points)
					     :y (cadr points)))
		    (end-point (make-point :x (caddr points)
					   :y (cadddr points))))
		(make-line-segment :start start-point :end end-point)))
	  pairs-of-points))

(defun straightp (line-segment)
  (or (= (point-x (line-segment-start line-segment))
	     (point-x (line-segment-end line-segment)))
	  (= (point-y (line-segment-start line-segment))
	     (point-y (line-segment-end line-segment)))))

(defun line-points (line)
  (let* ((start (line-segment-start line))
	 (end (line-segment-end line))
	 (x0 (point-x start))
	 (y0 (point-y start))
	 (x1 (point-x end))
	 (y1 (point-y end)))
    (cond ((and (/= x1 x0) (/= y1 y0))
	    (loop for x in (alexandria:iota (1+ (abs (- x1 x0)))
					    :start x0
					    :step (signum (- x1 x0)))
		  for y in (alexandria:iota (1+ (abs (- y1 y0)))
					    :start y0
					    :step (signum (- y1 y0)))
		  collect (make-point :x x :y y)))
	  ((/= 0 (- x1 x0))
	   (loop for x in (alexandria:iota (1+ (abs (- x1 x0)))
					   :start x0
					   :step (signum (- x1 x0)))
		 collect (make-point :x x :y y0)))
	  ((/= 0 (- y1 y0))
	   (loop for y in (alexandria:iota (1+ (abs (- y1 y0)))
					       :start y0
					       :step (signum (- y1 y0)))
		 collect (make-point :x x0 :y y)))
	  (t (error "Neither horizontal nor vertical")))))

(defun draw-point (map point)
  (incf (gethash (cons (point-x point) (point-y point)) map 0)))

(defun draw (lines)
  (let ((crossings (make-hash-table :test #'equal)))
    (loop for line in lines
	  do (loop for point in (line-points line)
		   do (draw-point crossings point)))
    crossings))

(defun count-crossings (drawing)
  (let ((count 0))
    (loop for value being the hash-values of drawing
	  do (when (> value 1) (incf count)))
    count))

(defun part1 (input)
  (count-crossings (draw (remove-if-not #'straightp (create-line-segments input)))))

(defun part2 (input)
  (count-crossings (draw (create-line-segments input))))
