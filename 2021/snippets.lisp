;; REGEX
(ql:quickload :cl-ppcre)
(defparameter move-regex "(up|forward|down) (\\d+)")
(defun parse-command (x) 
    (cl-ppcre::register-groups-bind 
        (move size) 
        (move-regex x) 
        (list move 
            (parse-integer size))))
