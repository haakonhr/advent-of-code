(load "utils.lisp")
(ql:quickload 'uiop)
(ql:quickload :cl-ppcre)

(defparameter *test* '(("forward" 5)
		       ("down" 5)
		       ("forward" 8)
		       ("up" 3)
		       ("down" 8)
		       ("forward" 2)))

(defun parse (line)
  (let ((res (cl-ppcre:split " " line)))
    (cons (car res) (parse-integer (cadr res)))))

(defun parse (s) ;; s is a multiline string copy pasted from input - in CL, "" is already multiline
  (let ((lines (cl-ppcre:split "\\n" s)))
    (loop for ln in lines
          collect (let ((res (cl-ppcre:split " " ln)))
                    (cons (car res) (read-from-string (cadr res)))))))

(defparameter *input* (mapcar #'parse (uiop:read-file-lines "day2-input")))

;; Part 1
(defun compute-position (moves)
  (let ((depth 0)
	(horizontal 0))
    (mapcar (lambda (x)
	      (cond ((string= (car x) "forward")
		     (setq horizontal (+ horizontal (cdr x))))
		    ((string= (car x) "down")
		     (setq depth (+ depth (cdr x))))
		    ((string= (car x) "up")
		     (setq depth (- depth (cdr x))))
		    (t (print "Something went awry!"))))
	    moves)
    (* horizontal depth)))

;; Part 2
(defun compute-position (moves)
  (let ((aim 0)
	(depth 0)
	(horizontal 0))
    (mapcar (lambda (x)
	      (cond ((string= (car x) "forward")
		     (setq horizontal (+ horizontal (cdr x)))
		     (setq depth (+ depth (* (cdr x) aim))))
		    ((string= (car x) "down")
		     (setq aim (+ aim (cdr x))))
		    ((string= (car x) "up")
		     (setq aim (- aim (cdr x))))
		    (t (print "Something went awry!"))))
	    moves)
    (* horizontal depth)))
