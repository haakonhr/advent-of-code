(ql:quickload 'uiop)
(load "utils.lisp")

;; Parsing
(defun binary->list (line)
  (let ((len (length line)))
    (loop for i from 0 to (1- len)
	  collect (- (char-int (char line i)) 48))))

(defparameter *test* (mapcar #'binary->list (uiop:read-file-lines "day3-test")))
(defparameter *test* (uiop:read-file-lines "day3-test"))
(defparameter *input* (uiop:read-file-lines "day3-input"))

;; Part 1

(defun get-ith-digits (binary-digit-strings)
  (let ((len (length (car binary-digit-strings))))
    (loop for i from 0 to (1- len)
	  collect (mapcar (lambda (s) (char->binary (char s i))) binary-digit-strings))))

(defun most-common (digits)
  (let ((len (length digits)))
    (if (> (sum digits) (/ len 2))
	1
	0)))

(defun least-common (digits)
  (let ((len (length digits)))
    (if (> (sum digits) (/ len 2))
	0
	1)))

(defun gamma (binary-digits)
  (mapcar #'most-common (get-ith-digits binary-digits)))

(defun epsilon (binary-digits)
  (mapcar #'least-common (get-ith-digits binary-digits)))

(defun list->string)

;; Maybe try working with decimal numbers instead and relying on shifts and exponents?

;; Part 2
(defun most-common-ith (digits i comparison)
  (let ((len (length digits))
	(digits-position-i (mapcar (lambda (s) (char->binary (char s i))) digits)))
    (if (funcall comparison (sum digits-position-i) (/ len 2))
	1
	0)))

(defun bit-criteria (number-string pos value)
  (if (equal (char->binary (char number-string pos)) value)
      t
      nil))

(defun filter-until-one (digits pos comparison)
  ;; (when (>= pos (length digits))
  ;;   (print "WTF?")
  ;;     digits)
  (let ((criteria-value (most-common-ith digits pos comparison)))
    (labels ((criteria (s)
	       (bit-criteria s pos criteria-value)))
      (if digits
	  (let ((filtered-digits (remove-if-not #'criteria digits)))
	    (if (> (length filtered-digits) 1)
		(filter-until-one filtered-digits (1+ pos) comparison)
		(car (last filtered-digits))))))))
