(ql:quickload 'uiop)
(ql:quickload 'cl-ppcre)
(load "utils.lisp")

(defparameter *test* 
  (mapcar #'parse-integer (cl-ppcre:all-matches-as-strings "(\\d+)" "16,1,2,0,4,2,7,1,2,14")))
(defparameter *input* 
  (mapcar #'parse-integer (cl-ppcre:all-matches-as-strings "(\\d+)" (uiop:read-file-line "7-input")))) 


;; Part 1

(defun fuel-consumption-1 (a b)
  (abs (- b a)))

(defun align (crab-positions pos fuel-func)
  (let ((total-movement 0))
    (loop for crab-pos in crab-positions
	  ;; do (incf total-movement (abs (- crab-pos pos)))
	  do (incf total-movement (funcall fuel-func crab-pos pos))
	  )
    total-movement))

(defun min-align (crab-positions fuel-func)
  (let* ((left (reduce #'min crab-positions))
	 (right (reduce #'max crab-positions))
	 (num-crabs (length crab-positions))
	 (curr-min (* (funcall fuel-func right left) num-crabs))
	 (best-pos 0))
    (loop for horizontal-pos from left to right
	  do (when (< (align crab-positions horizontal-pos fuel-func) curr-min)
	       (progn
		 (setf curr-min (align crab-positions horizontal-pos fuel-func))
		 (setf best-pos horizontal-pos))))
    (cons best-pos curr-min)
    ))

;; Part 2
(defun fuel-consumption-2 (a b)
  (reduce #'+ (loop for i from 1 to (abs (- b a)) 
		    collect i)))

(defun fuel-consumption-3 (a b)
  (let ((distance (abs (- a b))))
    (/ (* distance (1+ distance)) 2)))
