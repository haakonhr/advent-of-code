import itertools
import click


@click.command()
@click.argument("input", type=click.File("r"))
def main(input):
    lines = [int(l.strip()) for l in input.readlines()]
    for a, b, c in itertools.combinations(lines, 3):
        if a + b + c == 2020:
            print(a * b * c)
            break


if __name__ == "__main__":
    main()
