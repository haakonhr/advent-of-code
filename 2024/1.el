;; Read example-1, which contains data like this:
;; 3   4
;; 4   3
;; 2   5
;; 1   3
;; 3   9
;; 3   3

(defun read-and-sort-columns (filename)
  (with-temp-buffer
    (insert-file-contents filename)
    (let (col1 col2)
      (goto-char (point-min))
      (while (not (eobp))
        (let ((line (buffer-substring-no-properties
                    (line-beginning-position)
                    (line-end-position))))
          (when (string-match "\\([0-9]+\\)\\s-+\\([0-9]+\\)" line)
            (push (string-to-number (match-string 1 line)) col1)
            (push (string-to-number (match-string 2 line)) col2)))
        (forward-line 1))
      (list (sort col1 '<) (sort col2 '<)))))

(setq input (read-and-sort-columns "input-1"))

;; Part 1: Use the input and calculate the sum of the differences.
(let* ((col1 (car input))
       (col2 (cadr input))
       (total 0))
  (while (and col1 col2)
    (setq total (+ total (abs (- (car col1) (car col2))))
          col1 (cdr col1)
          col2 (cdr col2)))
  total)

;; Part 2: For each number in the first list, count the number of occurences of that number in the second list and add the product of the number and the number of occurences to a similarity score.

(defun naive-part-2 (input)
  (let* ((col1 (car input))
	 (col2 (cadr input))
	 (similarity-score 0))
    (dolist (num1 col1)
      (let ((count 0))
	(dolist (num2 col2)
          (when (= num1 num2)
            (setq count (1+ count))))
	(setq similarity-score (+ similarity-score (* num1 count)))))
    similarity-score))
(naive-part-2 input)

;; Improve the function and cache results so that the next time a number occurs it doesn't need to be recomputed

(dolist (num '(1 2 3 4 5))
  (prin1 (* num num)))

(mapcar (lambda (num) (* num num)) '(1 2 3 4 5))

(defun part-2-w-caching (input)
  (let* ((col1 (car input))
	 (col2 (cadr input))
	 (cache (make-hash-table))
	 (similarity-score 0))
    (dolist (num1 col1)
      (unless (gethash num1 cache)
	(let ((count 0))
	  (dolist (num2 col2)
	    (when (= num1 num2)
	      (setq count (1+ count))))
	  (puthash num1 count cache)))
      (setq similarity-score (+ similarity-score (* num1 (gethash num1 cache)))))
    similarity-score))
(part-2-w-caching input)

;; Let's compare the two
(benchmark 10 (+ 2 3))"Elapsed time: 0.000005s"
(benchmark 50 (naive-part-2 input))"Elapsed time: 0.000020s"
(benchmark 50 (part-2-w-caching input))"Elapsed time: 0.000019s""Elapsed time: 0.000007s"
