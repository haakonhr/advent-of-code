(defun read-file-contents (filename)
  "Read and return contents of FILENAME as a string."
  (with-temp-buffer
    (insert-file-contents filename)
    (buffer-string)))

(defun read-file-lines (filename)
  "Read FILENAME and return list of lines."
  (with-temp-buffer
    (insert-file-contents filename)
    (split-string (buffer-string) "\n" t)))

(defun read-file-stream (filename callback)
  "Read FILENAME and process each line with CALLBACK."
  (with-temp-buffer
    (insert-file-contents filename)
    (goto-char (point-min))
    (while (not (eobp))
      (funcall callback (buffer-substring-no-properties
                        (line-beginning-position)
                        (line-end-position)))
      (forward-line 1))))
